█████████████████████████████████████████████████████████████████████████ █░░░░░░░░░░░░░░█░░░░░░░░██░░░░░░░░█░░░░░░░░░░░░░░█░░░░░░██████████░░░░░░█ █░░▄▀▄▀▄▀▄▀▄▀░░█░░▄▀▄▀░░██░░▄▀▄▀░░█░░▄▀▄▀▄▀▄▀▄▀░░█░░▄▀░░░░░░░░░░░░░░▄▀░░█ █░░▄▀░░░░░░▄▀░░█░░░░▄▀░░██░░▄▀░░░░█░░░░░░▄▀░░░░░░█░░▄▀▄▀▄▀▄▀▄▀▄▀▄▀▄▀▄▀░░█ █░░▄▀░░██░░▄▀░░███░░▄▀▄▀░░▄▀▄▀░░███████░░▄▀░░█████░░▄▀░░░░░░▄▀░░░░░░▄▀░░█ █░░▄▀░░░░░░▄▀░░███░░░░▄▀▄▀▄▀░░░░███████░░▄▀░░█████░░▄▀░░██░░▄▀░░██░░▄▀░░█ █░░▄▀▄▀▄▀▄▀▄▀░░█████░░░░▄▀░░░░█████████░░▄▀░░█████░░▄▀░░██░░▄▀░░██░░▄▀░░█ █░░▄▀░░░░░░░░░░███████░░▄▀░░███████████░░▄▀░░█████░░▄▀░░██░░░░░░██░░▄▀░░█ █░░▄▀░░███████████████░░▄▀░░███████████░░▄▀░░█████░░▄▀░░██████████░░▄▀░░█ █░░▄▀░░███████████████░░▄▀░░███████████░░▄▀░░█████░░▄▀░░██████████░░▄▀░░█ █░░▄▀░░███████████████░░▄▀░░███████████░░▄▀░░█████░░▄▀░░██████████░░▄▀░░█ █░░░░░░███████████████░░░░░░███████████░░░░░░█████░░░░░░██████████░░░░░░█ █████████████████████████████████████████████████████████████████████████

An ![Awesome](https://cdn.rawgit.com/sindresorhus/awesome/d7305f38d29fed78fa85652e3a63e154dd8e8829/media/badge.svg) Python ATM Simulator

## About
An python atm simulator with a gui made with tkinter this program is licensed under GPL license see the license tab for more info.

## Installation
For installation write the following commands in terminal/powershell:

* _pip3 install tk_
* _pip3 install tcl_
* _To get Good font styles install the font in the directry /Orbitron/Orbitron-VariableFont_wght.ttf(optional)_
* _python3 main.py_

### Extra steps for linux/macosx

#### commands for linux:
    sudo apt-get install python3-tk
    python3 main.py
#### commands for macosx:
    brew install tcl-tk
    python3 main.py

## License

[![GPL](https://licensebuttons.net/l/GPL/2.0/88x62.png)](./LICENSE.md)

Copyright (C) 2021  sanatg

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.