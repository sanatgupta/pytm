#!/usr/bin/python


#█████████████████████████████████████████████████████████████████████████
#█░░░░░░░░░░░░░░█░░░░░░░░██░░░░░░░░█░░░░░░░░░░░░░░█░░░░░░██████████░░░░░░█
#█░░▄▀▄▀▄▀▄▀▄▀░░█░░▄▀▄▀░░██░░▄▀▄▀░░█░░▄▀▄▀▄▀▄▀▄▀░░█░░▄▀░░░░░░░░░░░░░░▄▀░░█
#█░░▄▀░░░░░░▄▀░░█░░░░▄▀░░██░░▄▀░░░░█░░░░░░▄▀░░░░░░█░░▄▀▄▀▄▀▄▀▄▀▄▀▄▀▄▀▄▀░░█
#█░░▄▀░░██░░▄▀░░███░░▄▀▄▀░░▄▀▄▀░░███████░░▄▀░░█████░░▄▀░░░░░░▄▀░░░░░░▄▀░░█
#█░░▄▀░░░░░░▄▀░░███░░░░▄▀▄▀▄▀░░░░███████░░▄▀░░█████░░▄▀░░██░░▄▀░░██░░▄▀░░█
#█░░▄▀▄▀▄▀▄▀▄▀░░█████░░░░▄▀░░░░█████████░░▄▀░░█████░░▄▀░░██░░▄▀░░██░░▄▀░░█
#█░░▄▀░░░░░░░░░░███████░░▄▀░░███████████░░▄▀░░█████░░▄▀░░██░░░░░░██░░▄▀░░█
#█░░▄▀░░███████████████░░▄▀░░███████████░░▄▀░░█████░░▄▀░░██████████░░▄▀░░█
#█░░▄▀░░███████████████░░▄▀░░███████████░░▄▀░░█████░░▄▀░░██████████░░▄▀░░█
#█░░▄▀░░███████████████░░▄▀░░███████████░░▄▀░░█████░░▄▀░░██████████░░▄▀░░█
#█░░░░░░███████████████░░░░░░███████████░░░░░░█████░░░░░░██████████░░░░░░█
#█████████████████████████████████████████████████████████████████████████ 

#A Python ATM Simulator

#this code should not be used again unless you credit the author @sanatg

#instructions to run the code are written in readme.md as libraries need to be installed

#Pytm main file
import os
import tkinter as tk  # python 3
from sys import platform
from tkinter import font
from tkinter.constants import S # python 3
from tkinter_custom_button import TkinterCustomButton
import time
current_balance = 1000
correct_password = 0
#import Tkinter as tk     # python 2
#import tkFont as tkfont  # python 2


#starting and setting up starting app this part was copy paste
class SampleApp(tk.Tk):

    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)
        #text variable dictionary to store and sync the value
        self.shared_data = {'Balance':tk.IntVar()}
        
        # the container is where we'll stack a bunch of frames
        # on top of each other, then the one we want visible
        # will be raised above the others
        container = tk.Frame(self)
        container.pack(side="top", fill="both", expand=True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        self.frames = {}
        for F in (StartPage,MenuPage,WithdrawPage,DepositPage,BalancePage):
            page_name = F.__name__
            frame = F(parent=container, controller=self)
            self.frames[page_name] = frame

            # put all of the pages in the same location;
            # the one on the top of the stacking order
            # will be the one that is visible.
            frame.grid(row=0, column=0, sticky="nsew")

        self.show_frame("StartPage")

    def show_frame(self, page_name):
        '''Show a frame for the given page name'''
        frame = self.frames[page_name]
        frame.tkraise()



class StartPage(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent,bg='#4B8BBE')
        self.controller = controller
        
        self.controller.title('Pytm -atm simulator')
        #checking platform compatibility for zoom
        if platform == "linux" or platform == "linux2":
            #linux
            self.controller.attributes('-zoomed', True)
        elif platform == "darwin":
            #mac os 
            self.controller.state('zoomed')
        elif platform == "win32":
            #windows
           self.controller.state('zoomed')

        #Setting program image and getting it from working directry
        self.controller.iconphoto(False,
        tk.PhotoImage(file=os.path.abspath(os.getcwd())+"/atm-machine.png")
        )
       
        #text or label
       
        self.heading_label = tk.Label(self,
                                                     text='Pytm- atm simulator',
                                                     font=('orbitron',45,'bold'),
                                                     foreground='#ffffff',
                                                     background='#4B8BBE')
        self.heading_label.pack(pady=25)
        space_label2 = tk.Label(self,height=4,bg='#4B8BBE')
        space_label2.pack()
        self.password_label = tk.Label(self,
                                                      text='Enter your card pin',
                                                      font=('orbitron',13),
                                                      bg='#4B8BBE',
                                                      fg='white')
        self.password_label.pack(pady=10)

        my_password = tk.StringVar()

        self.password_input_box = tk.Entry(self,textvariable=my_password,font=('orbitron',12),width=26)
        self.password_input_box.focus_set()

        self.password_input_box.pack(ipady=9)
        def handle_focus_in(_):
            self.password_input_box.configure(fg='black',show='*')
            
        self.password_input_box.bind('<FocusIn>',handle_focus_in)

        space_label = tk.Label(self,height=1,bg='#4B8BBE')
        space_label.pack()
        #authenticating and checking user password
        def check_password():
            #test passwd for now
            if my_password.get() == '123':
                my_password.set('')
                controller.show_frame('MenuPage')
                incorrect_password_label['text'] = ''
            else:
                my_password.set('')
                incorrect_password_label["text"]='cardpin not valid try again!'
                global correct_password
                correct_password = correct_password +1
            
            if correct_password == 3:
                exit()

        self.button_1 = TkinterCustomButton(master=self,
                                            bg_color=None,
                                            fg_color="#155885",
                                            hover_color="#a1bccf",
                                            text_font=None,
                                            text="CONTINUE",
                                            text_color="white",
                                            corner_radius=5,
                                            width=350,
                                            height=60,
                                            hover=True,
                                            command=check_password)
        self.button_1.pack()
        #cardpin not valid try again!
        incorrect_password_label = tk.Label(self,text="",font=('orbitron',12),bg='#5499C7',fg="red",anchor='n')
        incorrect_password_label.pack(fill='both',expand=True)
        bottom_frame = tk.Frame(self,relief='solid',borderwidth=3,background='white',border=0)
        bottom_frame.pack(fill='x',side='bottom')
        #visa icon        
        visa_photo = tk.PhotoImage(file='visa.png')
        visa_label = tk.Label(bottom_frame,image=visa_photo)
        visa_label.pack(side='left')
        visa_label.image = visa_photo
        #mastercard icon
        mastercard_photo = tk.PhotoImage(file='mastercard.png')
        mastercard_label = tk.Label(bottom_frame,image=mastercard_photo)
        mastercard_label.pack(side='left')
        mastercard_label.image = mastercard_photo
        #american express icon
        american_express_photo = tk.PhotoImage(file='american-express.png')
        american_express_label = tk.Label(bottom_frame,image=american_express_photo)
        american_express_label.pack(side='left')
        american_express_label.image = american_express_photo
        #time function
        def time_func():
            current_time = time.strftime('%I:%M %p').lstrip('0').replace(' 0',' ')
            time_label.config(text=current_time)
            #repeating function
            time_label.after(200,time_func)

        #time label
        time_label = tk.Label(bottom_frame,font=('orbitron',12))
        time_label.pack(side='right')

        time_func()
class MenuPage(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent,bg='#4B8BBE')
        self.controller = controller
        #text or label
        self.heading_label = tk.Label(self,
                                                     text='Pytm- atm simulator',
                                                     font=('orbitron',45,'bold'),
                                                     foreground='#ffffff',
                                                     background='#4B8BBE')
        self.heading_label.pack(pady=25)
        main_menu_label = tk.Label(self,
                                                           text='Main Menu',
                                                           font=('orbitron',13),
                                                           fg='white',
                                                           bg='#4B8BBE')
        main_menu_label.pack(pady=10)
        selection_label = tk.Label(self,text='Please select a option',font=('orbitron',10),fg='white',bg='#4B8BBE')
        selection_label.pack()
        button_frame = tk.Frame(self,bg="#5499C7")
        button_frame.pack(fill='both',expand=True)
        def withdraw():
            controller.show_frame('WithdrawPage')
        self.button_1 = TkinterCustomButton(master= button_frame,
                                            bg_color=None,
                                            fg_color="#155885",
                                            hover_color="#a1bccf",
                                            text_font=None,
                                            text="Withdraw",
                                            text_color="white",
                                            corner_radius=5,
                                            width=350,
                                            height=80,
                                            hover=True,
                                            command=withdraw,
                                            )
        def deposit():
            controller.show_frame('DepositPage')                                           
        self.button_1.pack(pady=20)
        self.button_2 = TkinterCustomButton(master= button_frame,
                                            bg_color=None,
                                            fg_color="#155885",
                                            hover_color="#a1bccf",
                                            text_font=None,
                                            text="Deposit",
                                            text_color="white",
                                            corner_radius=5,
                                            width=350,
                                            height=80,
                                            hover=True,
                                            command=deposit,)
        self.button_2.pack(pady=5)
        def balance():
            controller.show_frame('BalancePage')  
        self.button_3 = TkinterCustomButton(master= button_frame,
                                            bg_color=None,
                                            fg_color="#155885",
                                            hover_color="#a1bccf",
                                            text_font=None,
                                            text="Balance",
                                            text_color="white",
                                            corner_radius=5,
                                            width=350,
                                            height=80,
                                            hover=True,
                                            command=balance,)
        self.button_3.pack(pady=20)
        def exitapp():
            controller.show_frame('StartPage')
        self.button_4 = TkinterCustomButton(master=button_frame,
                                            bg_color=None,
                                            fg_color="#155885",
                                            hover_color="#a1bccf",
                                            text_font=None,
                                            text="Exit",
                                            text_color="white",
                                            corner_radius=5,
                                            width=350,
                                            height=80,
                                            hover=True,
                                            command=exitapp)
        self.button_4.pack(pady=5)
        bottom_frame = tk.Frame(self,relief='solid',borderwidth=3,background='white',border=0)
        bottom_frame.pack(fill='x',side='bottom')
        #visa icon        
        visa_photo = tk.PhotoImage(file='visa.png')
        visa_label = tk.Label(bottom_frame,image=visa_photo)
        visa_label.pack(side='left')
        visa_label.image = visa_photo
        #mastercard icon
        mastercard_photo = tk.PhotoImage(file='mastercard.png')
        mastercard_label = tk.Label(bottom_frame,image=mastercard_photo)
        mastercard_label.pack(side='left')
        mastercard_label.image = mastercard_photo
        #american express icon
        american_express_photo = tk.PhotoImage(file='american-express.png')
        american_express_label = tk.Label(bottom_frame,image=american_express_photo)
        american_express_label.pack(side='left')
        american_express_label.image = american_express_photo
        #time function
        def time_func():
            current_time = time.strftime('%I:%M %p').lstrip('0').replace(' 0',' ')
            time_label.config(text=current_time)
            #repeating function
            time_label.after(200,time_func)
            
        #time label
        time_label = tk.Label(bottom_frame,font=('orbitron',12))
        time_label.pack(side='right')

        time_func()



class WithdrawPage(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent,bg='#4B8BBE')
        self.controller = controller
                #text or label
        self.heading_label = tk.Label(self,
                                                     text='Pytm- atm simulator',
                                                     font=('orbitron',45,'bold'),
                                                     foreground='#ffffff',
                                                     background='#4B8BBE')
        self.heading_label.pack(pady=25)
        main_menu_label = tk.Label(self,
                                                           text='Choose the amount you want to withdraw!',
                                                           font=('orbitron',13),
                                                           fg='white',
                                                           bg='#4B8BBE')
        main_menu_label.pack(pady=10)
        #the button gradient frame
        button_frame = tk.Frame(self,bg="#5499C7")
        button_frame.pack(fill='both',expand=True)
        def withdraw(amount):
            global current_balance
            current_balance -= amount
            controller.shared_data['Balance'].set(current_balance)
            controller.show_frame('MenuPage')
        #withdraw buttons
        twenty_button = TkinterCustomButton(master=button_frame,
                                            bg_color=None,
                                            fg_color="#155885",
                                            hover_color="#a1bccf",
                                            text_font=None,
                                            text="20₹",
                                            text_color="white",
                                            width=350,
                                            height=80,
                                            hover=True,
                                            command=lambda:withdraw(20),)
        twenty_button.grid(row=0,column=0,pady=5)
        #40rupees
        forty_button = TkinterCustomButton(master=button_frame,
                                            bg_color=None,
                                            fg_color="#155885",
                                            hover_color="#a1bccf",
                                            text_font=None,
                                            text="40₹",
                                            text_color="white",
                                            width=350,
                                            height=80,
                                            hover=True,
                                            command=lambda:withdraw(40),)
        forty_button.grid(row=1,column=0,pady=5)
        #60rupees
        sixty_button = TkinterCustomButton(master=button_frame,
                                            bg_color=None,
                                            fg_color="#155885",
                                            hover_color="#a1bccf",
                                            text_font=None,             
                                            text="60₹",
                                            text_color="white",
                                            width=350,
                                            height=80,
                                            hover=True,
                                            command=lambda:withdraw(60),)
        sixty_button.grid(row=2,column=0,pady=5)
        #80rupees
        eighty_button = TkinterCustomButton(master=button_frame,
                                            bg_color=None,
                                            fg_color="#155885",
                                            hover_color="#a1bccf",
                                            text_font=None,             
                                            text="80₹",
                                            text_color="white",
                                            width=350,
                                            height=80,
                                            hover=True,
                                            command=lambda:withdraw(80),)
        eighty_button.grid(row=3,column=0,pady=5)
        #100rupees
        one_hundred_button = TkinterCustomButton(master=button_frame,
                                            bg_color=None,
                                            fg_color="#155885",
                                            hover_color="#a1bccf",
                                            text_font=None,             
                                            text="100₹",
                                            text_color="white",
                                            width=350,
                                            height=80,
                                            hover=True,
                                            command=lambda:withdraw(100),)
        one_hundred_button.grid(row=0,column=1,pady=5,padx=665)
        #200rupees
        two_hundred_button = TkinterCustomButton(master=button_frame,
                                            bg_color=None,
                                            fg_color="#155885",
                                            hover_color="#a1bccf",
                                            text_font=None,             
                                            text="200₹",
                                            text_color="white",
                                            width=350,
                                            height=80,
                                            hover=True,
                                            command=lambda:withdraw(200),)
        two_hundred_button.grid(row=1,column=1,pady=5,padx=665)
        #300rupees
        three_hundred_button = TkinterCustomButton(master=button_frame,
                                            bg_color=None,
                                            fg_color="#155885",
                                            hover_color="#a1bccf",
                                            text_font=None,             
                                            text="300₹",
                                            text_color="white",
                                            width=350,
                                            height=80,
                                            hover=True,
                                            command=lambda:withdraw(300),)
        three_hundred_button.grid(row=2,column=1,pady=5,padx=665)
        #400rupees
        four_hundred_button = TkinterCustomButton(master=button_frame,
                                            bg_color=None,
                                            fg_color="#155885",
                                            hover_color="#a1bccf",
                                            text_font=None,             
                                            text="400₹",
                                            text_color="white",
                                            width=350,
                                            height=80,
                                            hover=True,
                                            command=lambda:withdraw(400),)
        four_hundred_button.grid(row=2,column=1,pady=5,padx=665)
        cash = tk.StringVar()
        
        other_amount_entry = tk.Entry(button_frame,textvariable=cash,font=('orbitron',12),width=26)
        other_amount_entry.grid(row=3,column=1,pady=5,padx=665,ipady=25)
        def other_amount(_):
            global current_balance
            current_balance -= int(cash.get())
            cash.set('')
            controller.shared_data['Balance'].set(current_balance)
            controller.show_frame('MenuPage')
        other_amount_entry.bind('<Return>',other_amount)
        
 
        #command=lambda:withdraw(20),
        bottom_frame = tk.Frame(self,relief='solid',borderwidth=3,background='white',border=0)
        bottom_frame.pack(fill='x',side='bottom')
        #visa icon        
        visa_photo = tk.PhotoImage(file='visa.png')
        visa_label = tk.Label(bottom_frame,image=visa_photo)
        visa_label.pack(side='left')
        visa_label.image = visa_photo
        #mastercard icon
        mastercard_photo = tk.PhotoImage(file='mastercard.png')
        mastercard_label = tk.Label(bottom_frame,image=mastercard_photo)
        mastercard_label.pack(side='left')
        mastercard_label.image = mastercard_photo
        #american express icon
        american_express_photo = tk.PhotoImage(file='american-express.png')
        american_express_label = tk.Label(bottom_frame,image=american_express_photo)
        american_express_label.pack(side='left')
        american_express_label.image = american_express_photo
        #time function
        def time_func():
            current_time = time.strftime('%I:%M %p').lstrip('0').replace(' 0',' ')
            time_label.config(text=current_time)
            #repeating function
            time_label.after(200,time_func)
            
        #time label
        time_label = tk.Label(bottom_frame,font=('orbitron',12))
        time_label.pack(side='right')

        time_func()

class DepositPage(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent,bg='#4B8BBE')
        self.controller = controller
        #text or label
        self.heading_label = tk.Label(self,
                                                     text='Pytm- atm simulator',
                                                     font=('orbitron',45,'bold'),
                                                     foreground='#ffffff',
                                                     background='#4B8BBE')
        self.heading_label.pack(pady=25)
        space_label3 = tk.Label(self,height=4,bg='#4B8BBE')
        space_label3.pack()
        self.deposit_label = tk.Label(self,
                                                      text='Enter your deposit amount',
                                                      font=('orbitron',13),
                                                      bg='#4B8BBE',
                                                      fg='white')
        self.deposit_label.pack(pady=10)
        cash = tk.StringVar()
        deposit_entry = tk.Entry(self,textvariable=cash,font=('orbitron',12),width=26)
        deposit_entry.focus_set()
        deposit_entry.pack(ipady=9)
        space_label4 = tk.Label(self,height=1,bg='#4B8BBE')
        space_label4.pack()
        #increasing the deposit cash value
        def deposit_cash():
            global current_balance
            current_balance += int(cash.get())
            controller.shared_data['Balance'].set(current_balance)
            cash.set('')
            controller.show_frame('MenuPage')
            

        self.button_1 = TkinterCustomButton(master=self,
                                            bg_color=None,
                                            fg_color="#155885",
                                            hover_color="#a1bccf",
                                            text_font=None,
                                            text="ENTER",
                                            text_color="white",
                                            corner_radius=5,
                                            width=350,
                                            height=60,
                                            hover=True,
                                            command=deposit_cash)
        self.button_1.pack()
        button_frame = tk.Frame(self,bg="#5499C7")
        button_frame.pack(fill='both',expand=True)
        bottom_frame = tk.Frame(self,relief='solid',borderwidth=3,background='white',border=0)
        bottom_frame.pack(fill='x',side='bottom')
        #visa icon        
        visa_photo = tk.PhotoImage(file='visa.png')
        visa_label = tk.Label(bottom_frame,image=visa_photo)
        visa_label.pack(side='left')
        visa_label.image = visa_photo
        #mastercard icon
        mastercard_photo = tk.PhotoImage(file='mastercard.png')
        mastercard_label = tk.Label(bottom_frame,image=mastercard_photo)
        mastercard_label.pack(side='left')
        mastercard_label.image = mastercard_photo
        #american express icon
        american_express_photo = tk.PhotoImage(file='american-express.png')
        american_express_label = tk.Label(bottom_frame,image=american_express_photo)
        american_express_label.pack(side='left')
        american_express_label.image = american_express_photo
        #time function
        def time_func():
            current_time = time.strftime('%I:%M %p').lstrip('0').replace(' 0',' ')
            time_label.config(text=current_time)
            #repeating function
            time_label.after(200,time_func)

        #time label
        time_label = tk.Label(bottom_frame,font=('orbitron',12))
        time_label.pack(side='right')

        time_func()


class BalancePage(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent,bg='#4B8BBE')
        self.controller = controller
        #text or label
        self.heading_label = tk.Label(self,
                                                     text='Pytm- atm simulator',
                                                     font=('orbitron',45,'bold'),
                                                     foreground='#ffffff',
                                                     background='#4B8BBE')
        self.heading_label.pack(pady=25)
        self.amountleft_label = tk.Label(self,
                                                      text='Your total amount left',
                                                      font=('orbitron',13),
                                                      bg='#4B8BBE',
                                                      fg='white')
        self.amountleft_label.pack(pady=10)
        global current_balance
        controller.shared_data['Balance'].set(current_balance)

        balance_label = tk.Label(self,textvariable=controller.shared_data['Balance'],font=('orbitron',13),fg='white',bg='#4B8BBE')

        balance_label.pack()
        space_label5 = tk.Label(self,height=4,bg='#4B8BBE')
        space_label5.pack()
        button_frame = tk.Frame(self,bg="#5499C7")
        button_frame.pack(fill='both',expand=True)

        #return user to menu
        def return_menu_page():
            controller.show_frame('MenuPage')
        self.button_1 = TkinterCustomButton(master= button_frame,
                                            bg_color=None,
                                            fg_color="#155885",
                                            hover_color="#a1bccf",
                                            text_font=None,
                                            text="Return to menu",
                                            text_color="white",
                                            corner_radius=5,
                                            width=350,
                                            height=60,
                                            hover=True,
                                            command=return_menu_page)
        
        self.button_1.pack()

        def return_login_page():
            controller.show_frame('StartPage')

        self.button_1 = TkinterCustomButton(master= button_frame,
                                            bg_color=None,
                                            fg_color="#155885",
                                            hover_color="#a1bccf",
                                            text_font=None,
                                            text="Exit",
                                            text_color="white",
                                            corner_radius=5,
                                            width=350,
                                            height=60,
                                            hover=True,
                                            command=return_login_page)
        
        self.button_1.pack(pady=5)

        
        bottom_frame = tk.Frame(self,relief='solid',borderwidth=3,background='white',border=0)
        bottom_frame.pack(fill='x',side='bottom')
        #visa icon        
        visa_photo = tk.PhotoImage(file='visa.png')
        visa_label = tk.Label(bottom_frame,image=visa_photo)
        visa_label.pack(side='left')
        visa_label.image = visa_photo
        #mastercard icon
        mastercard_photo = tk.PhotoImage(file='mastercard.png')
        mastercard_label = tk.Label(bottom_frame,image=mastercard_photo)
        mastercard_label.pack(side='left')
        mastercard_label.image = mastercard_photo
        #american express icon
        american_express_photo = tk.PhotoImage(file='american-express.png')
        american_express_label = tk.Label(bottom_frame,image=american_express_photo)
        american_express_label.pack(side='left')
        american_express_label.image = american_express_photo
        #time function
        def time_func():
            current_time = time.strftime('%I:%M %p').lstrip('0').replace(' 0',' ')
            time_label.config(text=current_time)
            #repeating function
            time_label.after(200,time_func)

        #time label
        time_label = tk.Label(bottom_frame,font=('orbitron',12))
        time_label.pack(side='right')

        time_func()


if __name__ == "__main__":
    app = SampleApp()
    app.mainloop()
    